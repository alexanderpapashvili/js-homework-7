// exercise 1
list = [];
list[0] = ["Ryan", "Kieran", "Mark"];
list[1] = ["George", "Nick", "Tom", "Kate", "Annie"];
list[2] = ["James", "Will", "Jack", "Nate", "Edward"];

for (let i = 0; i < list.length; i++) {
  let friend = list[i].filter((item) => item.length == 4);
  console.log(friend);
}

// exercise 2
stats = [];
stats[0] = [19, 5, 42, 2, 77];
stats[1] = [10, 343445353, 3453445, 3453545353453];
stats[2] = [5, 8, 12, 19, 22];
stats[3] = [52, 76, 14, 12, 4];
stats[4] = [3, 87, 45, 12, 7];

function calc(arr) {
  arr.sort((a, b) => a - b);
  let sum = arr[0] + arr[1];
  console.log(`Sum: ${sum}. (${arr[0]}, ${arr[1]})`);
}

calc(stats[0]);
calc(stats[1]);
calc(stats[2]);
calc(stats[3]);
calc(stats[4]);
